# Napier

A scientific calculator for command line hackers.

## Documentation

* [Reference manual](https://gitlab.com/sarneaud/napier/-/blob/master/documentation/reference.adoc) (listing of builtins with details)
* [Cheatsheet](https://gitlab.com/sarneaud/napier/-/blob/master/documentation/cheatsheet.adoc) (short listing of builtins)
