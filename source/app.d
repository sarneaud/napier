import std.stdio;

import commandr;

import napier;
import napier.docs;
import napier.exception;
import napier.repl;
import napier.vrange;

enum kNapierVersion = "v0.2.0";

int main(string[] args)
{
	if (args.length == 1)
	{
		repl(kNapierVersion);
		return 0;
	}

	auto program = new Program(args[0], kNapierVersion)
		.summary("A scientific calculator CLI")
		.author("Simon Arneaud")
		.add(new Command("calc", "Evaluate an expression")
			.add(new Argument("expr", "expression to evaluate")))
		.add(new Command("for", "Evaluate expressions over ranges of values")
			.add(new Argument("ranges", "ranges to iterate over"))
			.add(new Argument("expr", "expression(s) to evaluate").repeating(true)))
		.add(new Command("gen", "Generate documentation and other files")
			.add(new Command("bash-completion", "Generate bash completion script"))
			.add(new Command("cheatsheet", "Generate Asciidoc Napier reference manual"))
			.add(new Command("reference", "Generate Asciidoc Napier reference manual")));
	auto options = program.parse(args);

	try
	{
		options.on("calc", (options) {
			auto symbols = makeGlobalScope();
			writeln(eval(options.arg("expr"), symbols));
		}).on("for", (options) {
			execFor(options.arg("ranges"), options.argAll("expr"));
		}).on("gen", (options) {
			options.on("bash-completion", (options) {
				import commandr.completion.bash;
				writeln(program.createBashCompletionScript());
			})
			.on("cheatsheet", (options) {
				writeCheatsheet(kNapierVersion);
			})
			.on("reference", (options) {
				writeReference(kNapierVersion);
			});
		});
	}
	catch (EvaluationException e)
	{
		stderr.writeln(e.msg);
		return 2;
	}
	return 0;
}

void execFor(string range_spec, string[] exprs)
{
	import std.algorithm : map;
	auto symbols = makeGlobalScope();
	auto ranges = parseVRanges(range_spec, symbols);
	void writeExprs()
	{
		import std.string : startsWith;
		foreach (idx, e; exprs)
		{
			if (idx != 0) write('\t');
			if (e.startsWith("#"))
			{
				auto comment = e[1..$];
				if (comment.startsWith(" ")) comment = comment[1..$];
				write(comment);
			}
			else
			{
				write(eval(e, symbols));
			}
		}
		writeln();
	}
	walkVRanges(ranges, symbols, "x", &writeExprs);
}
