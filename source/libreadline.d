module libreadline;

extern(C):

alias rl_completion_generator = extern(C) char* function(char* text, int state);
alias rl_command_func = extern(C) int function(int, int key);

extern __gshared const(char)* rl_readline_name, rl_basic_word_break_characters;
extern __gshared extern(C) rl_completion_generator rl_completion_entry_function;
extern __gshared char* rl_line_buffer;
extern __gshared int rl_point, rl_end;
extern __gshared int rl_readline_state;
extern __gshared char rl_completion_append_character;

enum RL_STATE_READCMD = 0x0000008;

char* readline(const(char)* prompt);
void add_history(const(char)* line);
int rl_initialize();

int rl_on_new_line();
void rl_replace_line(const(char)* text, int clear_undo);
void rl_redisplay();
int rl_reset_line_state();
void rl_free_line_state();
int rl_bind_key(int key, rl_command_func handler);
