module napier.constants;

import std.math;

struct ConstSpec
{
	string name, short_desc;
	double value;
}

immutable constants = [
	ConstSpec("E", "Euler's constant e", E),
	ConstSpec("inf", "Positive infinity", double.infinity),
	ConstSpec("nan", "IEEE754 floating point not-a-number", double.nan),
	ConstSpec("PI", "Circle circumference divided by diameter", PI),
];
