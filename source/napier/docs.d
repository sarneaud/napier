module napier.docs;

import std.stdio;

import std.algorithm;
import std.conv;
import std.range;

import napier;
import napier.constants;
import napier.functions;
import napier.macros;
import napier.operators;
import napier.utils;

void writeReference(string napier_version)
{
	write("# Napier Reference Manual " ~ napier_version ~ "\n:toc:\n\n");

	write("## Operators\n\n");
	foreach (idx, prec_group; operators.chunkBy!"a.prec_l".enumerate(1))
	{
		write("Precedence group ", idx, "\n");
		writeAsciiDocTable(prec_group[1].map!(o => [o.repr, o.short_desc]).array);
	}

	write("## Constants\n\n");
	foreach (c; constants)
	{
		write("### ", c.name, "\n", c.short_desc, ". Value: ", c.value, "\n\n");
	}

	write("## Macros\n\n");
	foreach (m; macros)
	{
		write("### ", m.repr, "\n", m.short_desc, "\n\n");
	}

	auto symbols = makeGlobalScope();
	foreach (fg; function_groups)
	{
		write("## ", fg.name, "\n", fg.short_desc, ".\n\n");
		foreach (f; fg.funcs)
		{
			write("### ", f.repr, "\n", f.short_desc, ".\n\n");
			if (!f.example_inputs.empty)
			{
				const(string[]) exampleRow(string example_args)
				{
					const expr = f.name ~ '(' ~ example_args ~ ')';
					const value = eval(expr, symbols);
					return [expr, value.to!string];
				}
				writeAsciiDocTable(f.example_inputs.map!exampleRow.array);
			}
		}
	}
}

void writeCheatsheet(string napier_version)
{
	write("# Napier Cheatsheet " ~ napier_version ~ "\n:toc:\n\n");

	write("## Operators\n\n");
	writeAsciiDocTable(operators.chunkBy!"a.prec_l".map!(g => [g[1].map!(o => o.repr).join(", "), g[1].map!(o => o.short_desc).join(", ")]).array);

	write("## Constants\n\n");
	writeAsciiDocTable(constants.map!(c => [c.name, c.short_desc, c.value.to!string]).array);

	write("## Macros\n\n");
	writeAsciiDocTable(macros.map!(m => [m.repr, m.short_desc]).array);

	auto symbols = makeGlobalScope();
	foreach (fg; function_groups)
	{
		write("## ", fg.name, "\n", fg.short_desc, ".\n\n");
		writeAsciiDocTable(fg.funcs.map!(f => [f.repr, f.short_desc]).array);
	}
}

void writeAsciiDocTable(const(string[][]) rows)
in
{
	assert (rows.length > 0);
	assert (rows.map!(r => r.length).uniq.walkLength == 1);
	assert (rows[0].length > 0);
}
do
{
	const nr = rows.length;
	const nc = rows[0].length;

	const max_lens = iota(nc - 1).map!(ci => rows.map!(r => r[ci]).maxGraphemeLength).array;
	write("|===\n");
	foreach (r; rows)
	{
		write("| ");
		foreach (ci; 0..nc-1)
		{
			write(r[ci], repeat(' ', max_lens[ci] - graphemeLength(r[ci])), " | ");
		}
		writeln(r[nc-1]);
	}
	write("|===\n\n");
}
