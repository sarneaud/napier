module napier.exception;

public import std.exception;

class TypeException : Exception
{
	mixin basicExceptionCtors;
}

class KeyException : Exception
{
	mixin basicExceptionCtors;
}

class EvaluationException : Exception
{
	mixin basicExceptionCtors;
}

class SyntaxException : EvaluationException
{
	mixin basicExceptionCtors;
}

class RuntimeException : EvaluationException
{
	mixin basicExceptionCtors;
}
