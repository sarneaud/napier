module napier.functions.arithmetic;

import napier.functions.wrapping;

enum name = "Arithmetic Functions";
enum short_desc = "Functions based on the four operations, powers and roots";
immutable(FunctionSpec[]) funcs;

shared static this()
{
	import std.math;

	funcs = [
		wrap!(cbrt, Desc("Cube root"), Examples(["2"])),
		wrap!(sqrt, Desc("Square root"), Examples(["2"])),
		wrap!(hypot, Desc("Length of hypotenuse of right-angled triangle with sides of length x and y"), Examples(["3, 4"])),
		wrap!(log, Desc("Natural (base e) logarithm"), Examples(["1", "E", "10"]), Name("ln")),
		wrap!(log10, Desc("Logarithm base 10"), Examples(["1", "10", "30"])),
		wrap!(log2, Desc("Logarithm base 2"), Examples(["1", "2", "64", "30"])),
	];
}
