module napier.functions.combinatorics;

import std.algorithm;
import std.conv;
import std.math;
import std.range;

import napier.exception;
import napier.functions.wrapping;
import napier.utils;

enum name = "Combinatoric Functions";
enum short_desc = "Functions for counting";
immutable(FunctionSpec[]) funcs;

shared static this()
{
	funcs = [
		wrap!bell,
		wrap!binomial,
		wrap!catalan,
		wrap!narayana,
	];
}

@Desc("The nth Bell number")
@Examples(["0", "1", "2", "3", "4"])
double bell(double n)
{
	enforce!RuntimeException(isIntegral(n) && n >= 0, text("bell(n) needs n to be non-negative integer. Got n=", n));
	if (n > bell_table.length) return double.infinity;
	return bell_table[n.to!size_t];
}

@Desc("Binomial coefficient")
@Examples(["4,2", "1/2,3"])
double binomial(double n, double k)
{
	auto ks = only(k, n-k).filter!(v => v >= 0 && isIntegral(v));
	enforce!RuntimeException(!ks.empty, text("binomial(n, k) needs k or n-k to be a non-negative integer. Got k=", k, ", n-k=", n-k));
	k = ks.minElement();
	if (n >= 0 && isIntegral(n) && k > n) return 0.0;
	return iota(1, k+1).map!(i => (n + 1 - i) / i).fold!"a*b"(1.0);
}

unittest
{
	assert (binomial(4, 2) == 6.0);
	assert (binomial(4, 0) == 1.0);
	assert (binomial(0, 0) == 1.0);
	assert (binomial(0.5, 3) == 1.0/16);
	assert (binomial(0.5, 3) == 1.0/16);
	assert (binomial(-1, 3) == -1.0);
	assert (binomial(2, 3) == 0.0);
	assert (binomial(1.5, 3) == -1.0/16.0);
	assert (binomial(1.5, -1.5) == -1.0/16.0);
}

@Desc("The nth Catalan number")
@Examples(["0", "1", "2", "3", "4"])
double catalan(double n)
{
	enforce!RuntimeException(isIntegral(n) && n >= 0, text("catalan(n) needs n to be a non-negative integer. Got n=", n));
	if (n > 520) return double.infinity;
	return 1.0 / (n+1) * binomial(2*n, n);
}

unittest
{
	assert (catalan(0) == 1);
	assert (catalan(1) == 1);
	assert (catalan(2) == 2);
	assert (catalan(3) == 5);
	assert (catalan(4) == 14);
}

@Desc("Narayana number")
@Examples(["4,2", "1/2,3"])
double narayana(double n, double k)
{
	enforce!RuntimeException(isIntegral(k) && k >= 1, text("narayana(n, k) needs k to be a positive integer. Got k=", k));
	const v = binomial(n, k - 1);
	return v * (n + 1 - k) / n * v / k;
}

unittest
{
	assert (narayana(2, 2) == 1);
	assert (narayana(7, 3) == 105);
	assert (narayana(8, 6) == 196);
}

private:
immutable bell_table = [
	0x1p+0,
	0x1p+0,
	0x1p+1,
	0x1.4p+2,
	0x1.ep+3,
	0x1.ap+5,
	0x1.96p+7,
	0x1.b68p+9,
	0x1.02cp+12,
	0x1.4a6cp+14,
	0x1.c507p+16,
	0x1.4b554p+19,
	0x1.012d74p+22,
	0x1.a5d215p+24,
	0x1.6c1c8f4p+27,
	0x1.49b91744p+30,
	0x1.385523a18p+33,
	0x1.34b2191acp+36,
	0x1.3d9de4381ep+39,
	0x1.5382a1bda04p+42,
	0x1.7857c09f976p+45,
	0x1.afe43eb4e24fp+48,
	0x1.002d586b19ddbp+52,
	0x1.39b807e239fe7p+55,
	0x1.8c174ea5301c5p+58,
	0x1.017e5552cc857p+62,
	0x1.5862cde0b8118p+65,
	0x1.d95586bc3d7acp+68,
	0x1.4df6a89c8c079p+72,
	0x1.e36ad4461d8d4p+75,
	0x1.669cb64cbbe0cp+79,
	0x1.1076836921fb9p+83,
	0x1.a7bafeb28588cp+86,
	0x1.50fe169759c82p+90,
	0x1.11f080ae37481p+94,
	0x1.c6f31d3d2776fp+97,
	0x1.81b14fc2aabcdp+101,
	0x1.4da573c5c4bdap+105,
	0x1.265c0590afb3ep+109,
	0x1.08bb80ce6caa9p+113,
	0x1.e52e9d1a84583p+116,
	0x1.c4d0c4baa678ap+120,
	0x1.ae3c3bb74722ep+124,
	0x1.9ffe51b41a34cp+128,
	0x1.992a131a87584p+132,
	0x1.993e7487c11dep+136,
	0x1.a01773e84861bp+140,
	0x1.ade7d55d7cc8bp+144,
	0x1.c33a52b4fcf46p+148,
	0x1.e0fa04140fbbbp+152,
	0x1.0441505f05224p+157,
	0x1.1ddd0ca274c4bp+161,
	0x1.3e9b0d007ee0fp+165,
	0x1.6838b1d86c798p+169,
	0x1.9d0b656effcb3p+173,
	0x1.e0352ded8eebfp+177,
	0x1.1af7401d48db8p+182,
	0x1.51f72c1f7dd8fp+186,
	0x1.98fee655b0cdcp+190,
	0x1.f5654a096112dp+194,
	0x1.37455491cca9p+199,
	0x1.8759ac56ad893p+203,
	0x1.f222f66aea651p+207,
	0x1.40e77c401bf88p+212,
	0x1.a26f572427cbbp+216,
	0x1.1409d0b7f7559p+221,
	0x1.7074c151ea348p+225,
	0x1.f17aa6f47323cp+229,
	0x1.53a77232fad66p+234,
	0x1.d4fd7aa6df675p+238,
	0x1.475ce500b0a8ap+243,
	0x1.cdfca50e1feadp+247,
	0x1.497d60c3cd983p+252,
	0x1.daf8167f354bdp+256,
	0x1.59ec8c1f55543p+261,
	0x1.fd15c143b6498p+265,
	0x1.7a6c5c93d95b9p+270,
	0x1.1c21891555377p+275,
	0x1.aee99c7beb5bap+279,
	0x1.49f8e01e28b59p+284,
	0x1.fe42caba9e78cp+288,
	0x1.8e5020b3303cep+293,
	0x1.39dfc26e4ad5cp+298,
	0x1.f34ea4fe7a524p+302,
	0x1.90d35e39e5a9dp+307,
	0x1.44b739db082a9p+312,
	0x1.0970c32714657p+317,
	0x1.b5dbcc17972ddp+321,
	0x1.6c55d9a8ebf31p+326,
	0x1.31d08aeb6a422p+331,
	0x1.02eb52a0b6ea2p+336,
	0x1.ba30ed06bc74ep+340,
	0x1.7cccc0aa9188ap+345,
	0x1.4aafc5b457ce9p+350,
	0x1.218e57d2172d1p+355,
	0x1.ff418a5bfc37cp+359,
	0x1.c707477b8625p+364,
	0x1.9840743f35c55p+369,
	0x1.7135a4dbe31efp+374,
	0x1.508ad0d30ff74p+379,
	0x1.352b2480b47a8p+384,
	0x1.1e39a34240d76p+389,
	0x1.0b056624466a7p+394,
	0x1.f601288c36988p+398,
	0x1.db72da4a6d17bp+403,
	0x1.c5a9d03c5e5f5p+408,
	0x1.b415184437714p+413,
	0x1.a6427425724c2p+418,
	0x1.9bd96b679807fp+423,
	0x1.94978c8d85736p+428,
	0x1.904da3cb5b694p+433,
	0x1.8eddbcd344f16p+438,
	0x1.9039d174da7f9p+443,
	0x1.9463109a99909p+448,
	0x1.9b69b16a70cddp+453,
	0x1.a56d4d88f22ebp+458,
	0x1.b29dc438aa55ap+463,
	0x1.c33caeb8c34d9p+468,
	0x1.d79f754b108a7p+473,
	0x1.f0321c19295a4p+478,
	0x1.06bd7422d255bp+484,
	0x1.180f843143688p+489,
	0x1.2c743c7ca818bp+494,
	0x1.4466b7551942p+499,
	0x1.607cd261dd6c3p+504,
	0x1.816d054256791p+509,
	0x1.a815b631b645fp+514,
	0x1.d586721bf1a69p+519,
	0x1.0585c59327ad3p+525,
	0x1.251e63a3f6a98p+530,
	0x1.4a87fd8f3b16bp+535,
	0x1.76f857d034c89p+540,
	0x1.abef24798facap+545,
	0x1.eb494c50cb223p+550,
	0x1.1bacd7d852c9p+556,
	0x1.498483e55b6bep+561,
	0x1.80ff8ceb19314p+566,
	0x1.c46c12a8a9005p+571,
	0x1.0b5a9b58b59a6p+577,
	0x1.3dc842a38d18fp+582,
	0x1.7bdcbd3a800b4p+587,
	0x1.c89fe6e46a854p+592,
	0x1.13fbb4188318dp+598,
	0x1.4f7598ed75655p+603,
	0x1.9a00478fa1b2ap+608,
	0x1.f7d9da376c2bdp+613,
	0x1.3746a5fed2e65p+619,
	0x1.82affa35c015fp+624,
	0x1.e2f2a4d096425p+629,
	0x1.2f31c22c45a8p+635,
	0x1.7eb557108b4ecp+640,
	0x1.e59e923b0f1fcp+645,
	0x1.35b6cf8190105p+651,
	0x1.8d1b9d93fc019p+656,
	0x1.ffcb0fb45a7d1p+661,
	0x1.4b7e5cbd16a3cp+667,
	0x1.af9d796ddf26ap+672,
	0x1.1a69ddac92256p+678,
	0x1.73708f342d76cp+683,
	0x1.eafa4e43f15b7p+688,
	0x1.461bf21f60512p+694,
	0x1.b359b3caa6514p+699,
	0x1.2405dd1ac5aep+705,
	0x1.89ae4d5fa37fcp+710,
	0x1.0aa78441ba1e5p+716,
	0x1.6af9d2ef8641ap+721,
	0x1.f07741275b142p+726,
	0x1.55259fb58394p+732,
	0x1.d7110d69723b1p+737,
	0x1.46c4b6a4cd3d2p+743,
	0x1.c779397ab6c44p+748,
	0x1.3eeb35a0fcf08p+754,
	0x1.c0aed7fcb201ep+759,
	0x1.3d14c6bdcd7bbp+765,
	0x1.c2377482d3dap+770,
	0x1.41161ed0e49bp+776,
	0x1.cc11120f7ba1cp+781,
	0x1.4b174d2c9281fp+787,
	0x1.deafbe6d254c3p+792,
	0x1.5b95e2a043552p+798,
	0x1.fb061c30e5f52p+803,
	0x1.736f02e3f0aadp+809,
	0x1.114cdae060a88p+815,
	0x1.93f27ac6537e3p+820,
	0x1.2bd2b98661bd7p+826,
	0x1.bf0123126292cp+831,
	0x1.4ea7357e69f8bp+837,
	0x1.f73a5e75164c9p+842,
	0x1.7bf8e8c2ade74p+848,
	0x1.201f51258998bp+854,
	0x1.b6cb322a85b7ap+859,
	0x1.4f880bd883007p+865,
	0x1.01a44676c292cp+871,
	0x1.8d4f8739c33c7p+876,
	0x1.339d3f0dd2858p+882,
	0x1.de4b8e20ddf58p+887,
	0x1.755ca70ca7f12p+893,
	0x1.24a33125ba957p+899,
	0x1.cc97bd27be7f4p+904,
	0x1.6bef2cc921f8ep+910,
	0x1.20b70156703ep+916,
	0x1.cbea286976f25p+921,
	0x1.6fc5b9402ae93p+927,
	0x1.274120d864929p+933,
	0x1.dbf052588c982p+938,
	0x1.8119f5e24756dp+944,
	0x1.38d12fb9171d5p+950,
	0x1.fe2ce35479bc2p+955,
	0x1.a1a1e0007c12ap+961,
	0x1.57310ffa15765p+967,
	0x1.1b198f379fb27p+973,
	0x1.d4d73430e0763p+978,
	0x1.85b2345829ae5p+984,
	0x1.4522bc201c474p+990,
	0x1.104a8a06dceadp+996,
	0x1.c9c75425a61d2p+1001,
	0x1.823ecf4c5daf5p+1007,
	0x1.47196c11c58e1p+1013,
	0x1.1608a194019d1p+1019,
];
