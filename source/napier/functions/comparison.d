module napier.functions.comparison;

import std.algorithm;
import std.math;
import std.range;

import napier.functions.wrapping;

enum name = "Comparison Functions";
enum short_desc = "Piecewise, step and comparison functions";
immutable(FunctionSpec[]) funcs;

shared static this()
{
	funcs = [
		wrap!(fabs, Desc("Absolute (non-negative) value"), Examples(["-3", "0", "3", "-inf"]), Name("abs")),
		wrap!allImpl,
		wrap!anyImpl,
		wrap!(ceil, Desc("Least integer greater than or equal to x"), Examples(["1", "2.7", "-2.7", "inf"])),
		wrap!coalesce,
		wrap!delta,
		wrap!(floor, Desc("Greatest integer less than or equal to x"), Examples(["1", "2.7", "-2.7", "inf"])),
		wrap!ifImpl,
		wrap!isFiniteImpl,
		wrap!isNaNImpl,
		wrap!maxImpl,
		wrap!minImpl,
		wrap!(delta, Desc("Maps zero to 1.0 and non-zero to 0.0 (alias of delta(x))"), Examples(), Name("not")),
		wrap!(round, Desc("Round to nearest integer"), Examples(["1", "2.7", "-2.7", "inf"])),
		wrap!(sgn!double, Desc("Signum function (-1, 0, 1 for negative, zero and positive x, respectively)"), Examples(["-PI", "0", "42", "inf", "-inf"])),
	];
}

@Name("all")
@Desc("1 if all values are non-zero, or 0 if any are zero")
@Examples(["1", "0", "2, 3", "4, 0", ""])
double allImpl(const(double)[] xs...)
{
	if (xs.canFind(0.0)) return 0.0;
	if (xs.any!(isNaN)) return double.nan;
	return 1.0;
}

unittest
{
	assert (allImpl() == 1.0);
	assert (allImpl(2.5) == 1.0);
	assert (allImpl(2.5, 2.5) == 1.0);
	assert (allImpl(2.5, double.infinity) == 1.0);
	assert (allImpl(0.0) == 0.0);
	assert (allImpl(0.0, 2.5) == 0.0);
	assert (allImpl(2.5, 0.0) == 0.0);
	assert (allImpl(double.infinity, 0.0) == 0.0);
	assert (allImpl(double.infinity, double.infinity) == 1.0);
	assert (isNaN(allImpl(double.nan)));
	assert (allImpl(2.5, 0.0, double.nan) == 0.0);
	assert (allImpl(0.0, double.nan) == 0.0);
}

@Name("any")
@Desc("1 if any value is non-zero, or 0 if all are zero")
@Examples(["1", "0", "2, 3", "4, 0", ""])
double anyImpl(const(double)[] xs...)
{
	if (xs.all!(x => x == 0.0)) return 0.0;
	if (xs.any!(x => !isNaN(x) && x != 0.0)) return 1.0;
	return double.nan;
}

unittest
{
	assert (anyImpl() == 0.0);
	assert (anyImpl(2.5) == 1.0);
	assert (anyImpl(2.5, 2.5) == 1.0);
	assert (anyImpl(0.0) == 0.0);
	assert (anyImpl(0.0, 2.5) == 1.0);
	assert (anyImpl(2.5, 0.0) == 1.0);
	assert (anyImpl(double.infinity, 0.0) == 1.0);
	assert (isNaN(anyImpl(double.nan)));
	assert (anyImpl(2.5, 0.0, double.nan) == 1.0);
	assert (isNaN(anyImpl(0.0, double.nan)));
}

@Desc("First non-NaN argument (else NaN if all arguments are NaN)")
@Examples(["nan, 42", "inf, nan", "nan", ""])
double coalesce(const(double)[] xs...)
{
	auto target = xs.find!(x => !isNaN(x));
	if (target.empty) return double.nan;
	return target.front;
}

unittest
{
	assert (isNaN(coalesce()));
	assert (isNaN(coalesce(double.nan)));
	assert (isNaN(coalesce(double.nan, double.nan)));
	assert (coalesce(double.nan, 2.0, 3.0) == 2.0);
	assert (coalesce(double.nan, double.nan, 3.0) == 3.0);
	assert (coalesce(1.0, double.nan, 3.0) == 1.0);
	assert (coalesce(double.infinity, double.nan, 3.0) == double.infinity);
}

@Desc("1 if x is zero and 0 for non-zero values")
double delta(double x)
{
	return ifImpl(x, 0.0, 1.0);
}

unittest
{
	assert (delta(0.0) == 1.0);
	assert (delta(0.5) == 0.0);
	assert (delta(-0.5) == 0.0);
	assert (delta(double.infinity) == 0.0);
	assert (isNaN(delta(double.nan)));
}

@Name("if")
@Desc("Return a if p is non-zero, or b if p is zero")
double ifImpl(double p, double a, double b = 0.0)
{
	if (isNaN(p)) return p;
	if (p == 0.0) return b;
	return a;
}

unittest
{
	assert (ifImpl(42.0, -5, 5) == -5);
	assert (ifImpl(double.infinity, -5, 5) == -5);
	assert (ifImpl(0.0, -5, 5) == 5);
	assert (ifImpl(0.0, -5) == 0.0);
	assert (isNaN(ifImpl(double.nan, -5, 5)));
	assert (isNaN(ifImpl(double.nan, -5)));
}

@Name("isFinite")
@Desc("1 if x is finite value, or 0 for positive and negative infinity")
@Examples(["0.0", "PI", "-inf", "inf"])
double isFiniteImpl(double x)
{
	if (isNaN(x)) return x;
	return isFinite(x) ? 1.0 : 0.0;
}

unittest
{
	assert (isFiniteImpl(0.0) == 1.0);
	assert (isFiniteImpl(42.0) == 1.0);
	assert (isFiniteImpl(-42.0) == 1.0);
	assert (isFiniteImpl(double.infinity) == 0.0);
	assert (isFiniteImpl(-double.infinity) == 0.0);
	assert (isNaN(isFiniteImpl(double.nan)));
}

@Name("isNaN")
@Desc("1 if x is NaN, else 0")
@Examples(["E", "nan"])
double isNaNImpl(double x)
{
	return isNaN(x) ? 1.0 : 0.0;
}

unittest
{
	assert (isNaNImpl(0.0) == 0.0);
	assert (isNaNImpl(double.infinity) == 0.0);
	assert (isNaNImpl(42.0) == 0.0);
	assert (isNaNImpl(double.nan) == 1.0);
}

@Name("max")
@Desc("Maximum of arguments")
@Examples(["-1, 5", "-1, nan", "inf, -inf", "4", ""])
double maxImpl(const(double)[] xs...)
{
	if (any!isNaN(xs)) return double.nan;
	return xs.fold!max(-double.infinity);
}

unittest
{
	assert (maxImpl() == -double.infinity);
	assert (maxImpl(1.0) == 1.0);
	assert (maxImpl(1.0, 2.0) == 2.0);
	assert (maxImpl(double.infinity, 1.0) == double.infinity);
	assert (maxImpl(-double.infinity, 1.0) == 1.0);
	// NaN treatment differs from fmax in D and C11
	assert (isNaN(maxImpl(double.nan, 1.0)));
	assert (isNaN(maxImpl(double.nan)));
	assert (isNaN(maxImpl(1.0, double.nan)));
}

@Name("min")
@Desc("Minimum of arguments")
@Examples(["-1, 5", "-1, nan", "inf, -inf", "4", ""])
double minImpl(const(double)[] xs...)
{
	if (any!isNaN(xs)) return double.nan;
	return xs.fold!min(double.infinity);
}

unittest
{
	assert (minImpl() == double.infinity);
	assert (minImpl(1.0) == 1.0);
	assert (minImpl(1.0, 2.0) == 1.0);
	assert (minImpl(double.infinity, 1.0) == 1.0);
	assert (minImpl(-double.infinity, 1.0) == -double.infinity);
	// NaN treatment differs from fmin in D and C11
	assert (isNaN(minImpl(double.nan, 1.0)));
	assert (isNaN(minImpl(double.nan)));
	assert (isNaN(minImpl(1.0, double.nan)));
}
