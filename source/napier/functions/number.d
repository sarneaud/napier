module napier.functions.number;

import std.algorithm;
import std.conv;
import std.math;
import std.range;

import napier.exception;
import napier.functions.wrapping;
import napier.utils;

enum name = "Number-theoretic Functions";
enum short_desc = "Functions for primality, divisors and other number theory";
immutable(FunctionSpec[]) funcs;

shared static this()
{
	funcs = [
		wrap!gcd,
		wrap!isPrime,
		wrap!lcm,
	];
}

@Desc("Greatest common divisor of a and b")
@Examples(["8,4", "12,9", "4,9"])
double gcd(double a, double b)
{
	if (isNaN(a) || isNaN(b)) return double.nan;
	enforce!RuntimeException(isIntegral(a) && isIntegral(b), text("gcd(a, b) needs integral arguments. Got a=", a, " b=", b));
	while (b != 0.0)
	{
		auto t = fmod(a, b);
		a = b;
		b = t;
	}
	return abs(a);
}

unittest
{
	assert (isNaN(gcd(double.nan,1)));
	assert (isNaN(gcd(1,double.nan)));
	assert (gcd(1,1) == 1.0);
	assert (gcd(4,4) == 4.0);
	assert (gcd(-4,4) == 4.0);
	assert (gcd(4,-4) == 4.0);
	assert (gcd(8,4) == 4.0);
	assert (gcd(12,9) == 3.0);
	assert (gcd(9,12) == 3.0);
	assert (gcd(9,4) == 1.0);
}

@Desc("Lowest common multiple of a and b")
@Examples(["8,4", "12,9", "4,9"])
double lcm(double a, double b)
{
	if (a == 0 && b == 0) return 0;
	try
	{
		return a / gcd(a, b) * b;
	}
	catch (RuntimeException e)
	{
		import std.string;
		throw new RuntimeException(e.msg.replace("gcd(", "lcm("));
	}
}

@Name("", ["n"])
@Desc("1.0 if n is prime, 0.0 if n is not prime")
@Examples(["7", "8", "2", "1", "0", "inf", "PI", "-7"])
double isPrime(double nd)
{
	// Miller-Rabin with a pre-computed table of witnesses known to cover the range we can sensibly work with.

	if (isNaN(nd)) return double.nan;
	if (nd > 1UL<<double.mant_dig) return double.nan;
	if (nd < 2 || !isIntegral(nd)) return 0.0;
	if (nd == 2.0) return 1.0;

	import core.bitop;
	auto n = nd.to!ulong;
	uint r = bsf(n-1);
	if (r == 0) return 0.0;
	const d = (n-1) >> r;

	static immutable(ulong)[] witnesses(ulong n) pure
	{
		// https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test#Testing_against_small_sets_of_bases
		static immutable all_witnesses = [2UL, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37];
		if (n < 2_047UL) return all_witnesses[0..1];
		if (n < 1_373_653UL) return all_witnesses[0..2];
		if (n < 25_326_001UL) return all_witnesses[0..3];
		if (n < 3_215_031_751UL) return all_witnesses[0..4];
		if (n < 2_152_302_898_747UL) return all_witnesses[0..5];
		if (n < 3_474_749_660_383UL) return all_witnesses[0..6];
		if (n < 341_550_071_728_321) return all_witnesses[0..7];
		return all_witnesses[];
	}
witness_loop: foreach (w; witnesses(n))
	{
		if (w >= n) return 1.0;
		auto x = powmod(w, d, n);
		if (x == 1 || x == n-1) continue witness_loop;
		foreach (j; 0..r-1)
		{
			x = powmod(x, 2UL, n);
			if (x == n-1) continue witness_loop;
		}
		return 0.0;
	}
	return 1.0;
}

unittest
{
	assert (isNaN(isPrime(double.nan)));
	assert (isNaN(isPrime(double.infinity)));
	assert (!isPrime(-double.infinity));
	assert (!isPrime(PI));
	assert (!isPrime(-7.0));
	assert (!isPrime(0.0));
	assert (!isPrime(1.0));

	assert (isPrime(2.0));
	assert (isPrime(3.0));
	assert (!isPrime(4.0));
	assert (isPrime(5.0));
	assert (!isPrime(6.0));
	assert (isPrime(7.0));
	assert (!isPrime(8.0));
	assert (!isPrime(9.0));

	assert (isPrime(9007199254740881.0));
	assert (!isPrime(9007199254740882.0));
}
