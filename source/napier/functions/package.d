module napier.functions;

import napier.functions.wrapping;

immutable(FunctionGroup[]) function_groups;
shared static this()
{
	immutable(FunctionGroup) getGroup(string module_name)()
	{
		mixin("import m = napier.functions.", module_name, ";");
		return immutable(FunctionGroup)(module_name, m.name, m.short_desc, m.funcs);
	}

	function_groups = [
		getGroup!"arithmetic",
		getGroup!"combinatorics",
		getGroup!"comparison",
		getGroup!"number",
		getGroup!"probability",
		getGroup!"special",
		getGroup!"trigonometry",
	];
}
