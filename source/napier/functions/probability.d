module napier.functions.probability;

import std.conv;

import dstats.distrib;

import napier.exception;
import napier.functions.wrapping;
import napier.utils;

enum name = "Probability Distributions";
enum short_desc = "Functions for working with probability distributions";
immutable(FunctionSpec[]) funcs;

shared static this()
{
	funcs = [
		wrap!(betaCDF, Desc("Beta cumulative distribution function"), Examples(), Name("", ["x", "α", "β"])),
		wrap!(invBetaCDF, Desc("Inverse of beta cumulative distribution function"), Examples(), Name("betaCDFInv", ["p", "α", "β"])),
		wrap!(betaPDF, Desc("Beta probability distribution function"), Examples(), Name("", ["x", "α", "β"])),
		wrap!binomialCDFImpl,
		wrap!binomialCDFInvImpl,
		wrap!binomialPMFImpl,
		wrap!(cauchyCDF, Desc("Cauchy cumulative distribution function"), Examples(), Name("", ["x", "X0", "γ"])),
		wrap!(invCauchyCDF, Desc("Inverse of Cauchy cumulative distribution function"), Examples(), Name("cauchyCDFInv", ["p", "X0", "γ"])),
		wrap!(cauchyPDF, Desc("Cauchy probability distribution function"), Examples(), Name("", ["x", "X0", "γ"])),
		wrap!(chiSquareCDF, Desc("Chi-square cumulative distribution function")),
		wrap!(chiSquarePDF, Desc("Chi-square probability distribution function")),
		wrap!(exponentialCDF, Desc("Exponential cumulative distribution function"), Examples(), Name("", ["x", "λ"])),
		wrap!(invExponentialCDF, Desc("Inverse of exponential cumulative distribution function"), Examples(), Name("exponentialCDFInv", ["p", "λ"])),
		wrap!(exponentialPDF, Desc("Exponential probability distribution function"), Examples(), Name("", ["x", "λ"])),
		wrap!(gammaCDF, Desc("Gamma cumulative distribution function")),
		wrap!(invGammaCDF, Desc("Inverse of gamma cumulative distribution function"), Examples(), Name("gammaCDFInv")),
		wrap!(gammaPDF, Desc("Gamma probability distribution function")),
		wrap!(laplaceCDF, Desc("Laplace cumulative distribution function"), Examples(), Name("", ["x", "μ", "b"])),
		wrap!(invLaplaceCDF, Desc("Inverse of Laplace cumulative distribution function"), Examples(), Name("laplaceCDFInv", ["p", "μ", "b"])),
		wrap!(laplacePDF, Desc("Laplace probability distribution function"), Examples(), Name("", ["x", "μ", "b"])),
		wrap!(logisticCDF, Desc("Logistic cumulative distribution function")),
		wrap!(logNormalCDF, Desc("Log-normal cumulative distribution function"), Examples(), Name("", ["x", "μ", "σ"])),
		wrap!(logNormalPDF, Desc("Log-normal probability distribution function"), Examples(), Name("", ["x", "μ", "σ"])),
		wrap!negBinomCDFImpl,
		wrap!negBinomCDFInvImpl,
		wrap!negBinomPMFImpl,
		wrap!(normalCDF, Desc("Normal cumulative distribution function"), Examples(), Name("", ["x", "μ", "σ"])),
		wrap!(invNormalCDF, Desc("Inverse normal cumulative distribution function"), Examples(), Name("normalCDFInv", ["p", "μ", "σ"])),
		wrap!(normalPDF, Desc("Normal probability distribution function"), Examples(), Name("", ["x", "μ", "σ"])),
		wrap!poissonCDFImpl,
		wrap!poissonCDFInvImpl,
		wrap!poissonPMFImpl,
		wrap!(uniformCDF, Desc("Uniform cumulative distribution function")),
		wrap!(uniformPDF, Desc("Uniform probability distribution function")),
		wrap!(weibullCDF, Desc("Weibull cumulative distribution function")),
		wrap!(weibullPDF, Desc("Weibull probability distribution function")),
	];
}

@Name("binomialCDF")
@Desc("Binomial cumulative distribution function")
double binomialCDFImpl(double k, double n, double p)
{
	enforce!RuntimeException(isIntegral(k) && k >= 0.0 && isIntegral(n) && n >= 0, text("Binomial distribution requires positive integral k and n. Got k=", k, " n=", n));
	return binomialCDF(k.to!ulong, n.to!ulong, p);
}

@Name("binomialCDFInv")
@Desc("Inverse of binomial cumulative distribution function")
double binomialCDFInvImpl(double x, double n, double p)
{
	enforce!RuntimeException(isIntegral(n) && n >= 0, text("Binomial distribution inverse requires positive integral n. Got n=", n));
	return invBinomialCDF(x, n.to!uint, p);  // FIXME: check upstream if this can be made ulong
}

@Name("binomialPMF")
@Desc("Binomial probability mass function")
double binomialPMFImpl(double k, double n, double p)
{
	enforce!RuntimeException(isIntegral(k) && k >= 0.0 && isIntegral(n) && n >= 0, text("Binomial distribution requires positive integral k and n. Got k=", k, " n=", n));
	return binomialPMF(k.to!ulong, n.to!ulong, p);
}

@Name("negBinomCDF")
@Desc("Negative binomial cumulative distribution function")
double negBinomCDFImpl(double k, double n, double p)
{
	enforce!RuntimeException(isIntegral(k) && k >= 0.0 && isIntegral(n) && n >= 0, text("Negative binomial distribution requires positive integral k and n. Got k=", k, " n=", n));
	return negBinomCDF(k.to!ulong, n.to!ulong, p);
}

@Name("negBinomialCDFInv")
@Desc("Inverse of negative binomial cumulative distribution function")
double negBinomCDFInvImpl(double x, double n, double p)
{
	enforce!RuntimeException(isIntegral(n) && n >= 0, text("Negative binomial distribution inverse requires positive integral n. Got n=", n));
	return invNegBinomCDF(x, n.to!ulong, p);
}

@Name("negBinomPMF")
@Desc("Negative binomial probability mass function")
double negBinomPMFImpl(double k, double n, double p)
{
	enforce!RuntimeException(isIntegral(k) && k >= 0.0 && isIntegral(n) && n >= 0, text("Negative binomial distribution requires positive integral k and n. Got k=", k, " n=", n));
	return negBinomPMF(k.to!ulong, n.to!ulong, p);
}

@Name("poissonCDF", ["k", "λ"])
@Desc("Poisson cumulative distribution function")
double poissonCDFImpl(double k, double lambda)
{
	enforce!RuntimeException(isIntegral(k) && k >= 0.0, text("Poisson distribution requires positive integral k. Got k=", k));
	return poissonCDF(k.to!ulong, lambda);
}

@Name("poissonCDFInv", ["k", "λ"])
@Desc("Inverse of Poisson cumulative distribution function")
double poissonCDFInvImpl(double k, double lambda)
{
	return invPoissonCDF(k, lambda);
}

@Name("poissonPMF", ["k", "λ"])
@Desc("Poisson probability mass function")
double poissonPMFImpl(double k, double lambda)
{
	enforce!RuntimeException(isIntegral(k) && k >= 0.0, text("Poisson distribution requires positive integral k. Got k=", k));
	return poissonPMF(k.to!ulong, lambda);
}
