module napier.functions.special;

import std.mathspecial;

import napier.exception;
import napier.functions.wrapping;

enum name = "Special Functions";
enum short_desc = "Functions with broad applications";
immutable(FunctionSpec[]) funcs;

shared static this()
{
	funcs = [
		wrap!(beta, Desc("Beta function")),
		wrap!(betaIncomplete, Desc("Incomplete beta function")),
		wrap!(betaIncompleteInverse, Desc("Inverse of incomplete beta function"), Examples(), Name("betaIncompleteInv")),
		wrap!(gamma, Desc("Gamma function"), Examples(["1", "2", "3", "1/2"])),
		wrap!(gammaIncomplete, Desc("Lower incomplete gamma function")),
		wrap!(logGamma, Desc("Natural logarithm of absolute gamma function"), Examples(), Name("lnAbsGamma")),
		wrap!(sgnGamma, Desc("Sign of gamma function")),
		wrap!(digamma, Desc("Derivative of natural logarithm of gamma function")),
	];
}
