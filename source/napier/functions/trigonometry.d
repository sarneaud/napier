module napier.functions.trigonometry;

import std.math;

import napier.functions.wrapping;

enum name = "Trigonometric Functions";
enum short_desc = "Functions based on angles and length ratios of triangles";
immutable(FunctionSpec[]) funcs;

shared static this()
{
	enum theta = ["θ"];
	funcs = [
		wrap!(acos, Desc("Inverse cosine (radians)"), Examples(["0", "1", "sqrt(0.5)"])),
		wrap!(acosh, Desc("Inverse hyperbolic cosine")),
		wrap!(asin, Desc("Inverse sine (radians)"), Examples(["0", "1", "sqrt(0.5)"])),
		wrap!(asinh, Desc("Inverse hyperbolic sine")),
		wrap!atanImpl,
		wrap!(atanh, Desc("Inverse hyperbolic tangent")),
		wrap!(cos, Desc("Cosine (radians)"), Examples(["0", "PI/4"]), Name("", theta)),
		wrap!(cosh, Desc("Hyperbolic cosine")),
		wrap!(sin, Desc("Sine (radians)"), Examples(["0", "PI/4"]), Name("", theta)),
		wrap!(sinh, Desc("Hyperbolic sine")),
		wrap!(tan, Desc("Tangent"), Examples(["0", "PI/4"]), Name("", theta)),
		wrap!(tanh, Desc("Hyperbolic tangent")),
	];
}

@Name("atan")
@Desc("Inverse tangent of o/a")
@Examples(["1", "sqrt(3), 3", "sqrt(3), -3", "-sqrt(3), -3"])
double atanImpl(double o, double a = 1.0)
{
	return atan2(o, a);
}
