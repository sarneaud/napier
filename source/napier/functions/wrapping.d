module napier.functions.wrapping;

import std.traits;

import napier.exception;

alias WrappedFunction = double delegate(const(double)[]);

struct FunctionSpec
{
	string repr, short_desc, long_desc;
	size_t name_length;
	const(string)[] example_inputs;
	WrappedFunction impl;

	string name() const pure
	{
		return repr[0..name_length];
	}
}
struct Desc { string short_desc, long_desc; }
struct Name { string name; string[] arg_names; }
struct Examples { const(string)[] example_inputs; }

struct FunctionGroup
{
	string id, name, short_desc;
	FunctionSpec[] funcs;
}

FunctionSpec wrap(alias f)()
{
	template Extract(alias f, T)
	{
		static if (hasUDA!(f, T))
		{
			enum Extract = getUDAs!(f, T)[0];
		}
		else
		{
			enum Extract = T();
		}
	}
	static assert (hasUDA!(f, Desc), "Wrapped functions must have a description");
	return wrap!(f, getUDAs!(f, Desc)[0], Extract!(f, Examples), Extract!(f, Name));
}

FunctionSpec wrap(alias f, Desc desc_uda, Examples examples_uda = Examples(), Name name_uda = Name())()
{
	import std.algorithm.iteration : map;
	import std.range : zip;
	import std.array : join;
	import std.conv : text, to;
	import std.meta : EraseAll, staticMap;
	template DefaultArgString(alias X)
	{
		static if (is(X == void))
		{
			enum DefaultArgString = "";
		}
		else
		{
			enum DefaultArgString = " = " ~ X.to!int.to!string;  // FIXME (CTFE double->string not supported yet)
		}
	}
	auto name = name_uda.name.length == 0 ? __traits(identifier, f) : name_uda.name;
	auto arg_names = name_uda.arg_names.length == 0 ? [ParameterIdentifierTuple!f] : name_uda.arg_names;
	auto default_strings = [staticMap!(DefaultArgString, ParameterDefaults!f)];
	if (variadicFunctionStyle!f == Variadic.typesafe)
	{
		assert (arg_names.length > 0);
		arg_names[$-1] = arg_names[$-1] ~ "...";
	}
	FunctionSpec ret = {
		repr: text(name, '(', zip(arg_names, default_strings).map!(t => t[0] ~ t[1]).join(", "), ')'),
		short_desc: desc_uda.short_desc,
		long_desc: desc_uda.long_desc,
		name_length: name.length,
		example_inputs: examples_uda.example_inputs,
		impl: makeWrappedImpl(&f, name, [EraseAll!(void, ParameterDefaults!f)])
	};
	return ret;
}

private:

WrappedFunction makeWrappedImpl(F)(F d_func, string name, const(double[]) default_args)
{
	import std.algorithm.comparison : min;
	import std.conv : text;
	import std.math : isNaN;
	enum is_variadic = variadicFunctionStyle!F == Variadic.typesafe;
	enum num_pos_args = Parameters!F.length - (is_variadic ? 1 : 0);
	double impl(const(double)[] vs)
	{
		enforce!TypeException(vs.length + default_args.length >= num_pos_args, text("at least ", num_pos_args - default_args.length, " arguments needed in call to '", name, "()' (got ", vs.length, ")"));
		enforce!TypeException(is_variadic || vs.length <= num_pos_args, text("function '", name, "()' takes a maximium of ", num_pos_args, " arguments (got ", vs.length, ")"));

		auto pos_vs = vs[0..min(vs.length, num_pos_args)];

		Parameters!F args = void;
		static foreach (j; 0..num_pos_args)
		{
			args[j] = j < vs.length ? vs[j] : default_args[$-(num_pos_args-j)];
		}
		static if (is_variadic) args[$-1] = vs[pos_vs.length..$];
		const ret = d_func(args);
		return isNaN(ret) ? double.nan : ret;  // Normalise NaNs
	}
	return &impl;
}
