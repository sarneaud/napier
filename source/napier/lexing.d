module napier.lexing;

import std.algorithm;
import std.conv;
import std.range;
import std.string;
import std.uni;
import std.utf;

import napier.exception;

enum TokenType
{
	identifier,
	number,
	operator,
	open_paren,
	close_paren,
	comma,
	colon,
	at,
	open_brace,
	close_brace,
}

struct Token
{
	TokenType type;
	string value;
}

struct Lexer
{
	pure:

	this(string s)
	{
		_orig = _input = s;
		if (!s.empty) popFront();
	}

	bool empty() const
	{
		return _input.empty && _front.value.ptr is null;
	}

	Token front() const
	{
		return _front;
	}

	void popFront()
	{
		_prev_idx = pos();
		_input = _input.stripLeft();
		if (_input.empty)
		{
			_front.value = string.init;
			return;
		}
		scope (exit) _input = _input.stripLeft();

		if (_input.length >= 2 && _input[0..2] in punc_tokens)
		{
			setFront(punc_tokens[_input[0..2]], 2);
		}
		else if (auto t = _input[0..1] in punc_tokens)
		{
			setFront(*t, 1);
		}
		else if (_input[0] == '_' || _input.byDchar.front.isAlpha())
		{
			const len = _input.byDchar.until!(c => c != '_' && !isAlphaNum(c)).codeLength!char;
			setFront(TokenType.identifier, len);
		}
		else if (_input[0] == '.' || _input.byDchar.front.isNumber())
		{
			const len = _input.byDchar.until!(c => c != '.' && !isNumber(c)).codeLength!char;
			setFront(TokenType.number, len);
		}
		else syntaxException("unrecognised character", false);
	}

	size_t pos() const pure
	{
		return _orig.length - _input.length;
	}

	void enforce(bool pred, lazy string msg, bool do_backtrack = true)
	{
		if (!pred) syntaxException(msg, do_backtrack);
	}

	void syntaxException(string msg, bool do_backtrack = true)
	{
		if (do_backtrack && _front.value.ptr !is null)
		{
			_input = _orig[_prev_idx..$];
		}
		// TODO: handle multi-line expressions
		throw new SyntaxException(text("Syntax error:\n", _orig, '\n', repeat(' ', pos), "^\n", msg));
	}

	void runtimeException(string msg, bool do_backtrack = true)
	{
		if (do_backtrack && _front.value.ptr !is null)
		{
			_input = _orig[_prev_idx..$];
		}
		// TODO: handle multi-line expressions
		throw new RuntimeException(text("Runtime error:\n", _orig, '\n', repeat(' ', pos), "^\n", msg));
	}

	private:

	void setFront(TokenType type, size_t len)
	{
		_front = Token(type, _input[0..len]);
		_input = _input[len..$];
	}

	string _input, _orig;
	size_t _prev_idx;
	Token _front;
}

private:

immutable(TokenType[string]) punc_tokens;
shared static this()
{
	with (TokenType)
	{
		punc_tokens = [
			"@": at,
			":": colon,
			",": comma,
			"{": open_brace,
			"}": close_brace,
			"(": open_paren,
			")": close_paren,
		];
		import napier.operators : operators;
		foreach (op; operators)
		{
			punc_tokens[op.repr] = operator;
		}
	}
}
