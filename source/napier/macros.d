module napier.macros;

import std.algorithm;
import std.conv;
import std.math;

import napier;
import napier.lexing;
import napier.vrange;

immutable(MacroSpec[]) macros = [
	MacroSpec("max{<expr>, <range>...}", "Maximum of expression evaluations over ranges", 3, &maxMacro),
	MacroSpec("min{<expr>, <range>...}", "Minimum of expression evaluations over ranges", 3, &minMacro),
	MacroSpec("prod{<expr>, <range>...}", "Product of expression evaluations over ranges", 4, &prodMacro),
	MacroSpec("sum{<expr>, <range>...}", "Sum of expression evaluations over ranges", 3, &sumMacro),
];

alias MacroImpl = double function(ref Lexer, ref Scope);

struct MacroSpec
{
	string repr, short_desc;
	size_t name_length;
	MacroImpl impl;

	string name() const pure
	{
		return repr[0..name_length];
	}
}

double maxMacro(ref Lexer tokens, ref Scope symbols)
{
	// NB: std.math.fmax behaves differently from std.algorithm.max with respect to NaNs
	// std.algorithm.max propagates NaNs, which is the behaviour wanted here
	double ret = -double.infinity;
	macroEvaluateOverRanges("max{}", tokens, symbols, (x) { ret = max(ret, x); });
	return ret;
}

double minMacro(ref Lexer tokens, ref Scope symbols)
{
	// NB: std.math.fmin behaves differently from std.algorithm.min with respect to NaNs
	// std.algorithm.min propagates NaNs, which is the behaviour wanted here
	double ret = double.infinity;
	macroEvaluateOverRanges("min{}", tokens, symbols, (x) { ret = min(ret, x); });
	return ret;
}

double prodMacro(ref Lexer tokens, ref Scope symbols)
{
	real ret = 1.0;
	macroEvaluateOverRanges("prod{}", tokens, symbols, (x) { ret *= x; });
	return ret;
}

double sumMacro(ref Lexer tokens, ref Scope symbols)
{
	// Neumaier variant of Kahan summation
	real ret = 0.0, c = 0.0;
	macroEvaluateOverRanges("sum{}", tokens, symbols, (x) {
			real t = ret + x;
			if (abs(ret) >= abs(x))
			{
				c += (ret - t) + x;
			}
			else
			{
				c += (x - t) + ret;
			}
			ret = t;
	});
	return ret + c;
}

private:

void macroEvaluateOverRanges(string macro_name, ref Lexer tokens, ref Scope symbols, void delegate(double) visitor)
{
	auto expr_start = tokens;
	skipArg(tokens, macro_name);
	tokens.enforce(!tokens.empty, text("expected } to end macro ", macro_name));
	VRange[] ranges;
	if (tokens.front.type == TokenType.comma)
	{
		tokens.popFront();
		ranges = parseVRanges(tokens, symbols);
	}
	tokens.enforce(!tokens.empty && tokens.front.type == TokenType.close_brace, text("expected } to end macro ", macro_name));
	tokens.popFront();

	walkVRanges(ranges, symbols, "j",  {
		auto expr_tokens = expr_start;
		const v = parseExpression(expr_tokens, symbols);
		visitor(v);
	});
}


void skipArg(ref Lexer tokens, string macro_name)
{
	uint brace_depth = 0, paren_depth = 0;
	with (TokenType)
	{
		while (!tokens.empty)
		{
			switch (tokens.front.type)
			{
				case open_brace:
					brace_depth++;
					break;

				case close_brace:
					if (paren_depth == 0 && brace_depth == 0) return;
					tokens.enforce(brace_depth > 0, text("unmatched } in macro '", macro_name, "'"));
					brace_depth--;
					break;

				case open_paren:
					paren_depth++;
					break;

				case close_paren:
					tokens.enforce(paren_depth > 0, text("unmatched ) in macro '", macro_name, "'"));
					paren_depth--;
					break;

				case comma:
					if (paren_depth == 0 && brace_depth == 0) return;
					break;

				default:
					break;
			}
			tokens.popFront();
		}
	}
}
