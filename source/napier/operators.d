module napier.operators;

import std.math;

immutable(OpSpec[]) operators = [
	OpSpec("==", "equality test", 0, 1, &binOpFunc!"=="),
	OpSpec("!=", "inequality test", 0, 1, &binOpFunc!"!="),
	OpSpec("<=", "less-than-or-equal test", 0, 1, &binOpFunc!"<="),
	OpSpec(">=", "greater-than-or-equal test", 0, 1, &binOpFunc!">="),
	OpSpec("<", "less-than test", 0, 1, &binOpFunc!"<"),
	OpSpec(">", "greater-than test", 0, 1, &binOpFunc!">"),
	OpSpec("+", "addition", 10, 11, &binOpFunc!"+"),
	OpSpec("-", "subraction", 10, 11, &binOpFunc!"-"),
	OpSpec("*", "multiplication", 20, 21, &binOpFunc!"*"),
	OpSpec("/", "division", 20, 21, &binOpFunc!"/"),
	OpSpec("%", "modulo", 20, 21, (a,b) { return fmod(a,b); }),
	OpSpec("", "multiplication by juxtaposition", 30, 31, &binOpFunc!"*"),
	OpSpec("^", "power", 41, 40, &binOpFunc!"^^"),
	OpSpec("!", "factorial", 50, 50, null),
];

alias BinOpImpl = double function(double, double);

struct OpSpec
{
	string repr, short_desc;
	int prec_l, prec_r;

	BinOpImpl impl;

	enum Associativity
	{
		left,
		unary,
		right,
	}
	Associativity associativity() const pure
	{
		if (prec_l < prec_r) return Associativity.left;
		if (prec_l > prec_r) return Associativity.right;
		return Associativity.unary;
	}
}

double binOpFunc(string op)(double a, double b)
{
	return mixin("a", op, "b");
}

double binOpBoolFunc(string op)(double a, double b)
{
	if (isNaN(a) || isNaN(b)) return double.nan;
	return mixin("a", op, "b") ? 1.0 : 0.0;
}

immutable(OpSpec*[string]) operator_table;
shared static this()
{
	import std.algorithm : map;
	import std.array : assocArray;
	import std.exception : assumeUnique;

	auto operator_table_init = assocArray(operators.map!(o => cast(string)o.repr), operators.map!((ref o) => &o));
	operator_table = operator_table_init.assumeUnique;
}
