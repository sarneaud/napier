module napier;

import std.algorithm;
import std.array;
import std.conv;
import std.math;
import std.mathspecial;
import std.range;
import std.string;

import napier.exception;
import napier.lexing;
import napier.macros;

double eval(string expr, ref Scope symbols)
{
	auto tokens = Lexer(expr);
	const result = parseExpression(tokens, symbols);
	tokens.enforce(tokens.empty, "extra tokens");
	return result;
}

struct Scope
{
	import napier.constants : ConstSpec;
	import napier.functions.wrapping : FunctionSpec, WrappedFunction;
	import napier.macros : MacroSpec;

	void addConsts(const(ConstSpec[]) consts) pure
	{
		foreach (c; consts) _vars[c.name] = c.value;
	}

	void addFuncs(const(FunctionSpec[]) funcs) pure
	{
		foreach (f; funcs) _funcs[f.name] = f.impl;
	}

	void addMacros(const(MacroSpec[]) macros) pure
	{
		foreach (m; macros) _macros[m.name] = m.impl;
	}

	double evalVariable(string name)
	{
		auto v = name in _vars;
		if (v !is null) return *v;

		throw new KeyException(text("Undefined variable '", name, "'"));
	}

	double evalFunction(string name, const(double)[] args)
	{
		auto func = name in _funcs;
		enforce!KeyException(func !is null, text("Undefined function '", name, "()'"));
		return (*func)(args);
	}

	double evalMacro(string name, ref Lexer tokens, ref Scope symbols)
	{
		auto m = name in _macros;
		enforce!KeyException(m !is null, text("Undefined macro '", name, "{}'"));
		return (*m)(tokens, symbols);
	}

	void setVar(string name, double v)
	{
		_vars[name] = v;
	}

	private:
	double[string] _vars;
	WrappedFunction[string] _funcs;
	MacroImpl[string] _macros;
}

Scope makeGlobalScope()
{
	import napier.constants : constants;
	import napier.macros : macros;
	import napier.functions : function_groups;
	Scope ret;
	ret.addConsts(constants);
	ret.addMacros(macros);
	foreach (fg; function_groups)
	{
		ret.addFuncs(fg.funcs);
	}
	return ret;
}

double parseValue(ref Lexer tokens, ref Scope symbols)
{
	tokens.enforce(!tokens.empty, "value expected");
	double sign = 1.0, value = 1.0;
	if (tokens.front.type == TokenType.operator)
	{
		switch (tokens.front.value)
		{
			case "+":
				break;
			case "-":
				sign = -1.0;
				break;
			default:
				tokens.syntaxException("value with optional unary + or - expected");
		}
		tokens.popFront();
		enforce(!tokens.empty);
	}

	switch (tokens.front.type)
	{
		case TokenType.open_paren:
			tokens.popFront();
			value = parseExpression(tokens, symbols);
			tokens.enforce(!tokens.empty && tokens.front.type == TokenType.close_paren, ") expected");
			tokens.popFront();
			break;

		case TokenType.number:
			try
			{
				value = tokens.front.value.to!double;
			}
			catch (ConvException e)
			{
				tokens.syntaxException(text("could not parse '", tokens.front.value, "' as floating point number: ", e.msg));
			}
			tokens.popFront();
			break;

		case TokenType.identifier:
			value = evalIdentifier(tokens, symbols);
			break;

		default:
			tokens.syntaxException("value expected");
	}

	if (!tokens.empty && tokens.front.type == TokenType.operator && tokens.front.value == "!")
	{
		tokens.popFront();
		value = gamma(value + 1);
	}
	return sign * value;
}

bool canStartExpression(ref const(Lexer) tokens) pure
{
	return !tokens.empty && tokens.front.type.among(TokenType.operator, TokenType.open_paren, TokenType.identifier, TokenType.number);
}

double parseExpression(ref Lexer tokens, ref Scope symbols, int at_prec = 0)
{
	import napier.operators : operator_table, OpSpec;
	double ret = parseValue(tokens, symbols);
	while (canStartExpression(tokens))
	{
		immutable(OpSpec)* op;
		if (tokens.front.type.among(TokenType.open_paren, TokenType.identifier, TokenType.number))
		{
			op = operator_table[""];
		}
		else
		{
			tokens.enforce(tokens.front.type == TokenType.operator, "binary operator expected");
			assert (tokens.front.value != "!");
			op = operator_table[tokens.front.value];
		}

		if (op.prec_l < at_prec) return ret;

		if (tokens.front.type == TokenType.operator) tokens.popFront();

		const rhs = parseExpression(tokens, symbols, op.prec_r);
		ret = op.impl(ret, rhs);
	}
	return ret;
}

package:

struct PrecSpec
{
	int l, r;
}

PrecSpec precedence(string operator) pure
{
	switch (operator)
	{
		case "+":
		case "-":
			return PrecSpec(1, 2);

		case "*":
		case "/":
			return PrecSpec(10, 11);

		case "^":
			return PrecSpec(21, 20);

		default:
			assert (false);
	}
}

double evalIdentifier(ref Lexer tokens, ref Scope symbols)
in (!tokens.empty && tokens.front.type == TokenType.identifier)
{
	auto orig_tokens = tokens;
	try
	{
		const id = tokens.front;
		tokens.popFront();
		if (tokens.empty || !tokens.front.type.among(TokenType.open_paren, TokenType.open_brace)) return symbols.evalVariable(id.value);
		if (tokens.front.type == TokenType.open_brace)
		{
			tokens.popFront();
			return symbols.evalMacro(id.value, tokens, symbols);
		}
		assert (tokens.front.type == TokenType.open_paren);
		tokens.popFront();
		auto args = appender!(double[]);
		while (!tokens.empty)
		{
			if (tokens.front.type == TokenType.close_paren)
			{
				tokens.popFront();
				return symbols.evalFunction(id.value, args[]);
			}
			args.put(parseExpression(tokens, symbols));
			if (!tokens.empty && tokens.front.type == TokenType.comma) tokens.popFront();
		}
		orig_tokens.syntaxException("no matching ) to close function call", false);
		assert (false);
	}
	catch (KeyException e)
	{
		orig_tokens.runtimeException(e.msg, true);
		assert (false);
	}
	catch (TypeException e)
	{
		orig_tokens.runtimeException(e.msg, true);
		assert (false);
	}
}

double evalBinOp(string operator, double a, double b) pure
{
	switch (operator)
	{
		case "+":
			return a + b;

		case "-":
			return a - b;

		case "*":
			return a * b;

		case "/":
			return a / b;

		case "^":
			return a ^^ b;

		default:
			assert (false);
	}
}
