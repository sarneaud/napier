module napier.repl;

import std.array : array;
import core.stdc.stdlib;
import std.algorithm;
import std.math;
import std.range;
import std.stdio;
import std.string;
import std.utf;

import libreadline;

import napier;
import napier.constants;
import napier.exception;
import napier.functions;
import napier.macros;
import napier.utils;

void repl(string napier_version)
{
	writeln("Napier ", napier_version);
	writeln("Enter !help for help.");

	auto symbols = makeGlobalScope();
	initRepl();

	foreach (line_c; generate!(() => readline("> ")).until(null))
	{
		scope (exit) free(line_c);
		auto line = line_c.fromStringz().strip().idup;
		if (line.empty) continue;
		add_history(line_c);
		if (line[0] == '#') continue;
		if (line[0] == '!')
		{
			if (line[1..$].strip().among("quit", "exit")) return;
			execCmd(line[1..$]);
			continue;
		}
		try
		{
			writeln(eval(line, symbols));
		}
		catch (EvaluationException e)
		{
			writeln(e.msg);
		}
	}
}

private:

immutable _all_commands = [
	"exit ",
	"help ",
	"quit ",
];

void execCmd(string cmd_line)
{
	auto args = cmd_line.split();
	if (args.empty) args = ["help"];
	switch (args[0])
	{
		case "help":
			help(args[1..$]);
			return;

		default:
			write("Unrecognised command '", args[0], "'.\nTry !help for help.\n");
	}
}

void help(string[] args)
{
	if (args.empty)
	{
		listHelpTopics(true);
		return;
	}

	const topics = args[0] in _help_table;
	if (topics is null)
	{
		write("Unknown help topic '", args[0], "'.\n");
		listHelpTopics(true);
		return;
	}

	foreach (topic; *topics)
	{
		topic.impl(args);
		writeln();
	}
}

void listHelpTopics(bool is_compact)
{
	write("Available help topics:\n\n");
	foreach (topic_name; _all_help_topics)
	{
		auto topics = _help_table[topic_name];
		foreach (topic; topics)
		{
			if (!is_compact || topic.show_in_compact_help) writeln("!help ", topic_name, "\n\t", topic.descr);
		}
	}
	if (is_compact)
	{
		writeln("!help [constant]\n\tShow help for [constant]");
		writeln("!help [function]\n\tShow help for [function]");
		writeln("!help [macro]\n\tShow help for [macro]");
	}
	write("\nEnter !quit to quit.\n");
}

void initRepl()
{

	rl_readline_name = "napier".ptr;
	rl_completion_entry_function = &rl_generator;
	rl_basic_word_break_characters = " \t\n+-*/^()!=<>".ptr;
	enforce(rl_initialize() == 0);

	version (Posix)
	{
		import core.sys.posix.signal;
		sigaction_t act;
		act.sa_handler = &on_sigint;
		sigaction(SIGINT, &act, null);
	}

	void addHelpTopic(T)(string topic_name, string descr, bool show_in_compact_help, const(T)* data, void function(const(T)*, string[] args) impl)
	{
		auto topic = HelpTopic(descr, show_in_compact_help, (string[] args) { impl(data, args); });
		_help_table.update(topic_name, { return [topic]; }, (HelpTopic[] i) { i ~= topic; return i; });
	}

	addHelpTopic("full", "List all help topics", true, null, (const(void)* d, string[] args) {
		listHelpTopics(false);
	});

	foreach (ref fg; function_groups)
	{
		import napier.functions.wrapping;
		static void groupHelp(const(FunctionGroup)* fg, string[] args)
		{
			const max_repr_len = fg.funcs.map!(f => f.repr).maxGraphemeLength;
			foreach (f; fg.funcs)
			{
				write(f.repr, repeat(' ', max_repr_len - graphemeLength(f.repr) + 1), f.short_desc, '\n');
			}
		}
		addHelpTopic(fg.id, fg.short_desc, true, &fg, &groupHelp);
		foreach (ref f; fg.funcs)
		{
			static void funcHelp(const(FunctionSpec)* f, string[] args)
			{
				writeln(f.repr);
				writeln(f.short_desc);
				if (!f.long_desc.empty)
				{
					write('\n', f.long_desc, ".\n\n");
				}
				return;
			}
			addHelpTopic(f.name, f.short_desc, false, &f, &funcHelp);
		}
	}

	foreach (ref c; constants)
	{
		static void constHelp(const(ConstSpec)* c, string[] args)
		{
			writeln(c.name, " (", c.value, ")\n", c.short_desc);
		}
		addHelpTopic(c.name, c.short_desc, false, &c, &constHelp);
	}

	addHelpTopic("constants", "All constants", true, null, (const(void)* d, string[] args) {
		const max_name_len = constants.map!(c => c.name).maxGraphemeLength;
		foreach (c; constants)
		{
			writeln(c.name, repeat(' ', max_name_len - graphemeLength(c.name) + 1), c.short_desc);
		}
	});

	foreach (ref m; macros)
	{
		static void macroHelp(const(MacroSpec)* m, string[] args)
		{
			writeln(m.repr, '\n', m.short_desc);
		}
		addHelpTopic(m.name, m.short_desc, false, &m, &macroHelp);
	}

	addHelpTopic("macros", "All macros", true, null, (const(void)* d, string[] args) {
		const max_name_len = macros.map!(m => m.name).maxGraphemeLength;
		foreach (m; macros)
		{
			writeln(m.name, repeat(' ', max_name_len - graphemeLength(m.name) + 1), m.short_desc);
		}
	});

	_all_symbols = allSymbols();
	_all_help_topics = _help_table.keys.sort.release;
}

string[] allSymbols()
{
	return chain(
		constants.map!(c => c.name),
		macros.map!(m => m.name ~ '{'),
		function_groups.map!(fg => fg.funcs.map!(f => f.name ~ '(')).joiner
	).array.sort.release;
}

const(string)[] prefixMatches(const(char[])[] dictionary, const(char[]) prefix)
in (isSorted(dictionary))
do
{
	const len = prefix.length;
	bool lt(const(char[]) a, const(char[]) b)
	{
		return a[0..min($, len)] < b[0..min($, len)];
	}
	return dictionary.assumeSorted!lt.equalRange(prefix).release;
}

extern(C) char* rl_generator(char* text_c, int state)
{
	static size_t j;
	static char[] text;
	static const(string)[] matches;
	rl_completion_append_character = '\0';
	if (!state)
	{
		j = 0;
		text = fromStringz(text_c);
		auto input_to_cursor = rl_line_buffer[0..rl_point];
		if (input_to_cursor.startsWith("!"))
		{
			if (auto pieces = input_to_cursor.byCodeUnit.findSplit(" "))
			{
				assert (pieces[0][0] == '!');
				switch (pieces[0][1..$].source)
				{
					case "help":
						matches = prefixMatches(_all_help_topics, text);
						break;

					default:
						break;
				}
			}
			else
			{
				matches = prefixMatches(_all_commands, text);
			}
		}
		else
		{
			matches = prefixMatches(_all_symbols, text);
		}
	}

	if (j == matches.length) return null;

	char* ret = cast(char*)enforce(malloc(matches[j].length+1));
	char* p = ret;
	foreach (char c; matches[j]) *p++ = c;
	*p++ = '\0';
	++j;
	return ret;
}

version (Posix)
{
	import core.sys.posix.signal;
	import core.sys.posix.unistd;
	extern(C) void on_sigint(int sig_num)
	{
		if (rl_readline_state & RL_STATE_READCMD)
		{
			import core.stdc.stdio : puts;
			puts("");
			rl_on_new_line();
			rl_replace_line("", 1);
			rl_redisplay();
		}
		else
		{
			kill(getpid(), SIGTERM);
			return;
		}
	}
}

alias HelpImpl = void delegate(string[] args);
struct HelpTopic
{
	string descr;
	bool show_in_compact_help;
	HelpImpl impl;
}

__gshared HelpTopic[][string] _help_table;
__gshared const(string)[] _all_symbols, _all_help_topics;
