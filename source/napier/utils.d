module napier.utils;

import std.algorithm;
import std.math;
import std.range;
import std.uni : byGrapheme;

bool isIntegral(double x) pure
{
	return isFinite(x) && trunc(x) == x;
}

unittest
{
	assert (isIntegral(1.0));
	assert (isIntegral(0.0));
	assert (isIntegral(-1.0));
	assert (!isIntegral(1.5));
	assert (!isIntegral(-1.5));
	assert (!isIntegral(double.infinity));
	assert (!isIntegral(double.nan));
}

size_t graphemeLength(S)(S s)
{
	return s.byGrapheme.walkLength;
}

unittest
{
	assert (graphemeLength("") == 0);
	assert (graphemeLength("a") == 1);
	assert (graphemeLength("θ") == 1);
}

size_t maxGraphemeLength(R)(R r)
{
	return r.map!graphemeLength.maxElement;
}

unittest
{
	assert (maxGraphemeLength(["", "a", "θ"]) == 1);
}
