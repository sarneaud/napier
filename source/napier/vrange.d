module napier.vrange;

import std.algorithm;
import std.conv;
import std.array;
import std.range;

import napier;
import napier.lexing;

VRange[] parseVRanges(string spec, ref Scope symbols)
{
	auto tokens = Lexer(spec);
	auto ret = parseVRanges(tokens, symbols);
	tokens.enforce(tokens.empty, "Ranges definition should be ,-separated list of start:delta:end@name ranges");
	return ret;
}

VRange[] parseVRanges(ref Lexer tokens, ref Scope symbols)
{
	auto ret = appender!(VRange[]);
	while (canStartExpression(tokens))
	{
		double[] args;
		while (args.length < 3)
		{
			double a = parseExpression(tokens, symbols);
			args ~= a;
			if (tokens.empty || tokens.front.type.among(TokenType.comma, TokenType.at, TokenType.close_brace)) break;
			tokens.enforce(tokens.front.type == TokenType.colon, ": expected to separate range arguments");
			tokens.popFront();
		}
		switch (args.length)
		{
			case 1:
				ret.put(VRange(1.0, args[0]));
				break;
			case 2:
				ret.put(VRange(args[0], args[1]));
				break;
			case 3:
				ret.put(VRange(args[0], args[2], args[1]));
				break;
			default:
				assert (false);
		}
		if (!tokens.empty)
		{
			if (tokens.front.type == TokenType.colon)
			{
				tokens.syntaxException("range spec takes maximum of three :-separated arguments");
				assert (false);
			}
			if (tokens.front.type == TokenType.at)
			{
				tokens.popFront();
				tokens.enforce(tokens.front.type == TokenType.identifier, "variable name expected after @");
				ret[][$-1]._name = tokens.front.value;
				tokens.popFront();
				if (tokens.empty) break;
			}
			if (tokens.front.type == TokenType.close_brace) break;
			tokens.enforce(tokens.front.type == TokenType.comma, ", expected to separate ranges");
			tokens.popFront();
		}
	}
	return ret[];
}

unittest
{
	import napier;
	bool test(string spec, double[][] expected, string[] names = [])
	{
		auto symbols = makeGlobalScope();
		auto ranges = parseVRanges(spec, symbols);
		if (ranges.length != expected.length) return false;
		return zip(ranges, expected).all!(t => equal(t[0], t[1])) && zip(ranges, names).all!(t => t[0].name == t[1]);
	}

	assert (test("", []));
	assert (test("3", [[1.0, 2, 3]]));
	assert (test("1:2", [[1.0, 2]]));
	assert (test("1:0.5:2", [[1.0, 1.5, 2]]));
	assert (test("1,2", [[1.0], [1.0, 2]]));
	assert (test("2,2:3", [[1.0, 2], [2.0, 3]]));

	assert (test("2/2", [[1.0]]));
	assert (test("2/2,2/2+1:3", [[1.0], [2.0, 3.0]]));

	assert (test("2/2@j", [[1.0]], ["j"]));
	assert (test("2/2@j,2/2+1:3", [[1.0], [2.0, 3.0]], ["j", ""]));
	assert (test("2/2,2/2+1:3@j", [[1.0], [2.0, 3.0]], ["", "j"]));
}

void walkVRanges(VRange[] ranges, ref Scope symbols, string default_var_name, void delegate() visitor)
{
	auto var_names = iota(ranges.length).map!(i => ranges[i].name.empty ? text(default_var_name, i+1) : ranges[i].name).array;
	void impl(size_t idx)
	{
		if (idx == ranges.length)
		{
			visitor();
			return;
		}

		foreach (v; ranges[idx])
		{
			if (idx == 0 && ranges[0].name.empty) symbols.setVar(default_var_name, v);
			symbols.setVar(var_names[idx], v);
			impl(idx+1);
		}
		ranges[idx].reset;
	}
	impl(0);
}

unittest
{
	import napier;
	bool test(string range_spec, double[][] expected)
	{
		auto symbols = makeGlobalScope();
		auto ranges = parseVRanges(range_spec, symbols);
		bool is_pass = true;
		size_t j = 0;
		uint num_checked = 0;
		walkVRanges(ranges, symbols, "z", {
			if (ranges.length > 0)
			{
				is_pass &= symbols.evalVariable("z") == symbols.evalVariable("z1");
			}
			foreach (k; 0..ranges.length)
			{
				is_pass &= symbols.evalVariable(text("z", k+1)) == expected[j][k];
				num_checked++;
			}
			j++;
		});
		is_pass &= num_checked == expected.map!(e => e.length).sum;
		return is_pass;
	}
	assert (test("", []));
	assert (test("3", [[1],[2],[3]]));
	assert (test("2,2", [[1,1],[1,2],[2,1],[2,2]]));
	assert (test("1,2", [[1,1],[1,2]]));
	assert (test("1,2,1", [[1,1,1],[1,2,1]]));
	assert (test("1,2,2", [[1,1,1],[1,1,2],[1,2,1],[1,2,2]]));
}

struct VRange
{
	this (double start, double end, double delta = 1) pure
	{
		import std.math : sgn;
		_start = start;
		_end = end;
		if (sgn(delta) * sgn(end - start) == -1) delta = -delta;
		_delta = delta;
	}

	string name() const pure
	{
		return _name;
	}

	double front() const pure
	{
		return _start + _n * _delta;
	}

	bool empty() const pure
	{
		if (_delta == 0.0) return false;
		if (_delta > 0) return front > _end;
		return front < _end;
	}

	void popFront() pure
	{
		_n++;
	}

	void reset() pure
	{
		_n = 0;
	}

	private:

	double _start, _end, _delta;
	ulong _n;
	string _name;
}

unittest
{
	assert (equal(VRange(1.0, 4.0), [1.0, 2, 3, 4]));
	assert (equal(VRange(4.0, 1.0), [4.0, 3, 2, 1]));
	assert (equal(VRange(1.0, 4.0, 0.5), [1.0, 1.5, 2, 2.5, 3, 3.5, 4]));
	assert (equal(VRange(4.0, 1.0, -0.5), [4.0, 3.5, 3, 2.5, 2, 1.5, 1]));
	assert (equal(VRange(4.0, 1.0, 0.5), [4.0, 3.5, 3, 2.5, 2, 1.5, 1]));
	assert (equal(VRange(4.0, 4.0), [4.0]));
	assert (equal(VRange(1.0, 4.0, 0.0).take(10), repeat(1.0, 10)));
}
